function createCard(name, location, description, pictureUrl, start, end) {
  let startFormat = new Date(start);
  let endFormat = new Date(end);
    return `
    <div class="col">
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top" alt="${name}">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">
            <small class="text-body-secondary">${startFormat.toLocaleDateString()} - ${endFormat.toLocaleDateString()}</small>
          </div>
        </div>
      </div>
    </div>
    `;
}

function Alert(errorCode) {
  return `
  <div class="alert alert-danger" role="alert">
    Frick! Error ${errorCode}!
    </div>
    `;

}





window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';


  try {
      const response = await fetch(url);

      if (!response.ok) {
          // Figure out what to do when the response is bad
          const alert = document.querySelector('h2')
          alert.innerHTML += Alert(response.status)
      } else {
          const data = await response.json();

          for (let conference of data.conferences){
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const name = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const start = details.conference.starts;
                  const end = details.conference.ends;
                  const location = details.conference.location.name;
                  let html = createCard(name,location, description,  pictureUrl, start, end);
                  let column = document.querySelector('.row');
                  column.innerHTML += html;
              }
          }
      }
  } catch (e) {
    console.error('@ Catch', e)
  }

});
