import Nav from './Nav.js';
import AttendeesList from './AttendeesList.js';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm.js';
import AttendConferenceForm from './AttendConferenceForm.js';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container">
      <LocationForm />
      <AttendeesList attendees={props.attendees} />
      <AttendConferenceForm />
      <ConferenceForm />


    </div>
    </>
  );
}

export default App;
